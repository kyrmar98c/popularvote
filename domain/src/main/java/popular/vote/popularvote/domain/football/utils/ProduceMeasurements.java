package popular.vote.popularvote.domain.football.utils;

public class ProduceMeasurements {

    private static final String HOME_TEAM_WINS = "1";
    private static final String AWAY_TEAM_WINS = "2";
    private static final String DRAW = "X";

    public static String determineResult(Integer homeTeamsGoals, Integer awayTeamGoals){
        if(homeTeamsGoals>awayTeamGoals){
            return HOME_TEAM_WINS;
        }
        if(awayTeamGoals>homeTeamsGoals){
            return AWAY_TEAM_WINS;
        }
        return DRAW;
    }
    public static String determineGoalsScored(Integer homeTeamsGoals, Integer awayTeamGoals){
        return String.valueOf(homeTeamsGoals+awayTeamGoals);
    }

}
