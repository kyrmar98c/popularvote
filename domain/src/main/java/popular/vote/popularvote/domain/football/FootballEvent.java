package popular.vote.popularvote.domain.football;

import lombok.Getter;
import lombok.Setter;
import popular.vote.popularvote.domain.Event;
import popular.vote.popularvote.domain.EventStatus;
import popular.vote.popularvote.domain.determinable.Determinable;
import popular.vote.popularvote.domain.determinable.DeterminablesDefinition;
import popular.vote.popularvote.domain.football.utils.ProduceMeasurements;
import java.util.HashMap;
import java.util.List;

@Getter
@Setter
public class FootballEvent extends Event {

    private String homeTeam;
    private String awayTeam;
    private Integer homeTeamGoals;
    private Integer awayTeamGoals;


    private final DeterminablesDefinition determinablesDefinition;


    public FootballEvent(Long id, List<Determinable> determinables, EventStatus status, Long externalReference,DeterminablesDefinition determinablesDefinition) {
        super(id, determinables,status,externalReference);
        this.determinablesDefinition = determinablesDefinition;
    }

    @Override
    public HashMap<String, String> generateContext() {
        HashMap<String,String> context = new HashMap<>();
        context.put("result", ProduceMeasurements.determineResult(getHomeTeamGoals(),getAwayTeamGoals()));
        context.put("goalsScored",ProduceMeasurements.determineGoalsScored(getHomeTeamGoals(),getAwayTeamGoals()));
        return context;
    }


    public void determineDeterminables(){
        HashMap<String,String> context = generateContext();
        getDeterminables().stream().forEach(determinable -> determinable.determineStatus(context));
    }

}
