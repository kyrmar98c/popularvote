package popular.vote.popularvote.domain.requirement.generic;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public abstract class Requirement {

    private String id;
    private String measurementIdentifier;
    private String measure;

    public abstract Boolean isMet(String measurement);

}
