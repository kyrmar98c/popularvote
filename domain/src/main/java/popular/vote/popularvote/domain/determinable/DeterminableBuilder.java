package popular.vote.popularvote.domain.determinable;

import lombok.Data;
import popular.vote.popularvote.domain.requirement.generic.Requirement;


import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class DeterminableBuilder {

    private String type;
    private List<String> requirements;

    public Determinable get(List<Requirement> requirements) {
        if ("and".equals(type)) {
            return new AndDeterminable(StatusDeterminable.TBD,null,requirements.stream().
                    filter(requirement -> this.requirements.contains(requirement.getId())).collect(Collectors.toList()), BigInteger.ZERO);
        }
        throw new AssertionError();
    }

}
