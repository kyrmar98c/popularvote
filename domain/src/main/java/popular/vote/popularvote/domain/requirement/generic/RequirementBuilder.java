package popular.vote.popularvote.domain.requirement.generic;

import lombok.Data;

@Data
public class RequirementBuilder {

    private String id;
    private String type;
    private String measureIdentifier;
    private String measure;


    public Requirement get() {
        if ("over".equals(type)) {
            return new OverRequirement(getId(),getMeasureIdentifier(),getMeasure());
        } else if ("under".equals(type)) {
            return new UnderRequirement(getId(),getMeasureIdentifier(),getMeasure());
        } else if ("result".equals(type)) {
            return new ResultRequirement(getId(),getMeasureIdentifier(),getMeasure());
        }
        throw new AssertionError();
    }

}
