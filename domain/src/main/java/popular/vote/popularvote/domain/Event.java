package popular.vote.popularvote.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import popular.vote.popularvote.domain.determinable.Determinable;

import java.util.HashMap;
import java.util.List;

@Data
@AllArgsConstructor
public abstract class Event {

    private Long id;
    private List<Determinable> determinables;
    private EventStatus status;
    private Long externalReference;

    abstract public HashMap<String,String> generateContext();

}
