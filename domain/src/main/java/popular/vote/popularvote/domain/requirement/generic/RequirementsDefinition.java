package popular.vote.popularvote.domain.requirement.generic;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@ConfigurationProperties
@Data
public class RequirementsDefinition {
    List<RequirementBuilder> requirementsBuilder;
    List<Requirement> requirements;

    @PostConstruct
    protected void postConstruct(){
        requirements = requirementsBuilder.stream().map(r -> r.get()).collect(Collectors.toList());
    }

}
