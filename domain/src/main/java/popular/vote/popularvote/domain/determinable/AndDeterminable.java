package popular.vote.popularvote.domain.determinable;

import popular.vote.popularvote.domain.requirement.generic.Requirement;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;


public class AndDeterminable extends Determinable {

    public AndDeterminable(Long id,StatusDeterminable status, String title, List<Requirement> requirements, BigInteger votes) {
        super(id,status, title, requirements,votes);
    }

    public AndDeterminable(StatusDeterminable status, String title, List<Requirement> requirements, BigInteger votes) {
        super(status, title, requirements,votes);
    }


    @Override
   public void determineStatus(HashMap<String, String> measurements) {
        if(getRequirements().stream().filter(requirement->!requirement.isMet(measurements.get(requirement.getMeasurementIdentifier())))
                .count()>0.0){
            setStatus(StatusDeterminable.DH);
        }
        else{
            setStatus(StatusDeterminable.H);
        }
    }

}
