package popular.vote.popularvote.domain.requirement.generic;

public class UnderRequirement extends Requirement {

    public UnderRequirement(String id,String measurementIdentifier, String measure) {
        super(id,measurementIdentifier,measure);
    }

    @Override
    public Boolean isMet(String measurement) {
        if(Long.valueOf(getMeasure())<Long.valueOf(measurement)){
            return false;
        }
        return true;
    }
}
