package popular.vote.popularvote.domain.requirement.generic;

public class ResultRequirement extends Requirement {

    public ResultRequirement(String id,String measurementIdentifier, String result) {
        super(id,measurementIdentifier,result);
    }

    @Override
    public Boolean isMet(String measurement) {
        if(getMeasure().equals(measurement)){
            return true;
        }
        return false;
    }

}
