package popular.vote.popularvote.domain.determinable;

public enum StatusDeterminable {
    // TBD  : To be determined
    // H : Happened
    //DH : Didn't happen
    TBD,H,DH
}
