package popular.vote.popularvote.domain.requirement.generic;

public class OverRequirement extends Requirement {

    public OverRequirement(String id ,String measurementIdentifier, String measure) {
        super(id,measurementIdentifier,measure);
    }

    @Override
    public Boolean isMet(String measurement) {
        if(Long.valueOf((getMeasure())) <Long.valueOf(measurement)){
            return true;
        }
        return false;
    }

}
