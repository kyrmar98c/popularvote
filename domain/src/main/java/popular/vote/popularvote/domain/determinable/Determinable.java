package popular.vote.popularvote.domain.determinable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import popular.vote.popularvote.domain.requirement.generic.Requirement;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;

/*
* Determinable models something that can be determined in a sport event for example if more than 5  goals are achieved
* Status of the Determinable can be TBD,H,DH
* Each determinable has a title. Is what the user will be seeing
* */

@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class Determinable {

    private Long id;
    private StatusDeterminable status;
    private String title;
    private List<Requirement> requirements;
    private BigInteger votes;

    public  Determinable(StatusDeterminable status, String title, List<Requirement> requirements,BigInteger votes){
        this.status = status;
        this.title = title;
        this.requirements = requirements;
        this.votes = votes;
    }


    public abstract void determineStatus(HashMap<String,String> measurements);

}
