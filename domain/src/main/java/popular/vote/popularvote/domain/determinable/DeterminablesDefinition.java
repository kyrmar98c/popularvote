package popular.vote.popularvote.domain.determinable;


import org.springframework.boot.context.properties.ConfigurationProperties;
import popular.vote.popularvote.domain.requirement.generic.RequirementsDefinition;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@ConfigurationProperties
public class DeterminablesDefinition {

    List<DeterminableBuilder> determinableBuilders;
    List<Determinable> determinables;

    @PostConstruct
    protected void postConstruct(RequirementsDefinition requirementsDefinition){
        determinables = determinableBuilders.stream().map(d -> d.get(requirementsDefinition.getRequirements())).collect(Collectors.toList());
    }

}
