package popular.vote.popularvote.services.scheduled.football.interfaces;

import popular.vote.popularvote.domain.Event;

import java.util.List;

public interface FootballApiService {

    List<Event> consumeUpcomingEvents(List<Integer> ids);

}
