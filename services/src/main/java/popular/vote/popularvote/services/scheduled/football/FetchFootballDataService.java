package popular.vote.popularvote.services.scheduled.football;

import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import popular.vote.popularvote.domain.Event;
import popular.vote.popularvote.services.scheduled.football.interfaces.FootballApiService;

import java.util.List;

// TODO Finish the implementation of the scheduled tasks

@Service
@AllArgsConstructor
public class FetchFootballDataService {

    private final FootballApiService footballApiService;

    @Scheduled(fixedRate = 30000000)
    @Async
    public void consumeUpcomingFootballEvents(){

        List<Event> events = footballApiService.consumeUpcomingEvents(null);
        //Take a list from db with externalReferences from all the events that have not finished
        //Filter consumed events from football adapter and keep only those that their externalReferences are not in the list
        //Save them to database



    }

    @Scheduled(fixedRate = 100000)
    @Async
    public void consumePossiblyFinishedFootballEvents(){

        // Take the id's of matches that their scheduled time is 2 hours in the past from current time
        // if there are more than 20 create patched of 20 ids
        // consume fixtures
        // for those fixtures finished update Events
        // save events in database


    }


}
