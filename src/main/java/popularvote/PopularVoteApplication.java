package popularvote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableMongoRepositories
public class PopularVoteApplication {

	public static void main(String[] args) {
		SpringApplication.run(PopularVoteApplication.class, args);
	}

	//TODO Implement mongodb adapter
	//TODO Create mongodb
	//TODO Find an efficient way to test end to end the application most probably running docker images
	//TODO Dockerize application
	//TODO Think about the FE part of the application
	//TODO Justify code with tests
	//TODO Create git repository in gitlab and push the code create the pipelines
	//TODO setup digital ocean environment to automatically deploy application
	//TODO Implement FE adapter and document exposed apis to swagger

}
