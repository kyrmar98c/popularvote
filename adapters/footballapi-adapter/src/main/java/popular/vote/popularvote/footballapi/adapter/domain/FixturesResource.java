package popular.vote.popularvote.footballapi.adapter.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class FixturesResource {

    @JsonProperty("get")
    private String apiCall;

    private Parameters parameters;

    private List<Error> errors;

    private Integer results;

    private Paging paging;

    private List<FootballEvent> response;

    @Data
    private static class Parameters {
        private String live;
    }

    @Data
    private static class Paging {
        private Integer current;
        private Integer total;
    }

    @Data
    private static class Error {
        private LocalDateTime time;
        private String bug;
        private String report;
    }

}
