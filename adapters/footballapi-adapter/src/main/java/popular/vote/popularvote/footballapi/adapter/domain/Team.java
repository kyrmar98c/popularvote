package popular.vote.popularvote.footballapi.adapter.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Team {

    private Long id;
    private String name;
    @JsonProperty("logo")
    private String logoUri;
    private Boolean winner;

}
