package popular.vote.popularvote.footballapi.adapter.client;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("footballApi")
@Data
public class RapidFootballApiConfiguration {
    private String baseUrl;
}
