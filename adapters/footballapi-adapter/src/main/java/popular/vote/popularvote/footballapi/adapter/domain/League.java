package popular.vote.popularvote.footballapi.adapter.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class League {

    private Long id;
    private String name;
    private String country;
    @JsonProperty("logo")
    private String logoUri;
    @JsonProperty("flag")
    private String flagUri;

}
