package popular.vote.popularvote.footballapi.adapter.services;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import popular.vote.popularvote.domain.Event;
import popular.vote.popularvote.footballapi.adapter.client.RapidFootballApiClient;
import popular.vote.popularvote.footballapi.adapter.domain.FixturesResource;
import popular.vote.popularvote.footballapi.adapter.mappers.FootballEventMapper;
import popular.vote.popularvote.services.scheduled.football.interfaces.FootballApiService;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class FootballApiServiceImpl implements FootballApiService {

   private final RapidFootballApiClient rapidFootballApiClient;

    @Override
    public List<Event> consumeUpcomingEvents(List<Integer> ids) {

        FixturesResource fixturesResource;

        if(ids == null){
            fixturesResource = rapidFootballApiClient.getUpcomingFixtures();
            return fixturesResource.getResponse().stream().map(footballEvent -> FootballEventMapper.mapper.map(footballEvent)).
                    collect(Collectors.toList());
        }


        return null;

    }

}
