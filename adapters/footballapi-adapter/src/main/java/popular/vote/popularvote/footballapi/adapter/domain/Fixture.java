package popular.vote.popularvote.footballapi.adapter.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.TimeZone;

@Data
public class Fixture {

    private Long id;
    private String referee;
    private TimeZone timeZone;
    private LocalDateTime date;
    private Long timestamp;
    private Period periods;
    private Venue venue;
    private Status status;

    @Data
    private static class Period {
        private Long first;
        private Long second;
    }

    @Data
    private static class Venue {
        private Long id;
        private String name;
        private String city;
    }

    @Data
    private static class Status {
        @JsonProperty("long")
        private String longS;
        @JsonProperty("short")
        private String shortS;
        private String elapsed;
    }

}
