package popular.vote.popularvote.footballapi.adapter.domain;

import lombok.Data;

@Data
public class FootballEvent {

    private Fixture fixture;
    private League league;
    private Teams teams;
    private Score goals;
    private AnalyticalScore score;

    @Data
    private static class Score {
        private Integer home;
        private Integer away;
    }

    @Data
    private static class AnalyticalScore {
       private Score halftime;
       private Score fulltime;
       private Score extratime;
       private Score penalty;
    }

    @Data
    private static class Teams {
        private Team home;
        private Team away;
    }

}
