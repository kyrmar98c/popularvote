package popular.vote.popularvote.footballapi.adapter.mappers;

import org.mapstruct.Mapper;

import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import popular.vote.popularvote.footballapi.adapter.domain.FootballEvent;

@Mapper
public interface FootballEventMapper {

    FootballEventMapper mapper = Mappers.getMapper(FootballEventMapper.class);

    //TODO carefully implement mapper with necessary information
    @Mapping(target = "externalReference", source = "footballEvent.fixture.id")
    popular.vote.popularvote.domain.football.FootballEvent map(FootballEvent footballEvent);

}
