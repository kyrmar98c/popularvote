package popular.vote.popularvote.footballapi.adapter.client;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import popular.vote.popularvote.footballapi.adapter.domain.FixturesResource;

import java.time.ZoneId;
import java.util.Date;

@Component
public class RapidFootballApiClient {

    private final WebClient client;
    private final String  X_RAPIDAPI_KEY = "";
    private final String  FIXTURES_PATH = "fixtures";

    public RapidFootballApiClient(RapidFootballApiConfiguration configuration){

        client= WebClient.builder().baseUrl(configuration.getBaseUrl()).build();

    }

    // Take Fixtures scheduled up to 7 days ahead from the current date
    public FixturesResource getUpcomingFixtures() {
        return client.get().uri(uriBuilder -> uriBuilder.path(FIXTURES_PATH).
                queryParam("from",( new Date()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate().atStartOfDay().toString())
                .queryParam("to",( new Date()).toInstant().atZone(ZoneId.systemDefault()).plusDays(7).toLocalDate().atStartOfDay().toString())
                .build()).header("x-rapidapi-key",X_RAPIDAPI_KEY).retrieve().bodyToMono(FixturesResource.class).block();
    }

    // Take Possibly finished fixtures passing a query param string "id-id-id"
    // Maximum ids that can be passed are 20
    public FixturesResource getPossiblyFinishedFixtures(String ids){
        return client.get().uri(uriBuilder -> uriBuilder.path(FIXTURES_PATH).queryParam("ids",ids)
                .build()).header("x-rapidapi-key",X_RAPIDAPI_KEY).retrieve().bodyToMono(FixturesResource.class).block();
    }

}
