package popularvote.adapters.mongodbadapter.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Prediction")
public class Prediction {

    @Id
    private String id;
    private String determinableId;
    private String userId;
}
