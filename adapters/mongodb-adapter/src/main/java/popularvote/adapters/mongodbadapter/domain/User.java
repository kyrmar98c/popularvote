package popularvote.adapters.mongodbadapter.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collation = "User")
public class User {

    @Id
    private String id;

    private String username;
    private String password;


}
